loop do
print "(R)ock, (S)cissor, (P)aper? "
c = gets.strip.capitalize

if c == "R"
    user_choice = :rock
elsif c == "S"
    user_choice = :scissor
elsif c == "P"
    user_choice = :paper
else
    puts "I can't understand what you want, sorry..."
    break
end

arr = [:rock, :scissor, :paper]
computer_choice = arr[rand(0..2)]

puts "User choice #{user_choice}"
puts "Computer choice #{computer_choice}"

matrix = [
    [:rock, :rock, :draw],
    [:scissor, :scissor, :draw],
    [:paper, :paper, :draw],

    [:rock, :scissor, :first_win],
    [:rock, :paper, :second_win],

    [:paper, :scissor, :second_win],
    [:paper, :rock, :first_win],

    [:scissor, :rock, :second_win],
    [:scissor, :paper, :first_win]
]

matrix.each do |item|
    if item[0] == user_choice && item[1] == computer_choice
        if item[2] == :first_win
            puts "User wins!"
        elsif item[2] == :second_win
            puts "Computer wins!"
        else
            puts "Draw"
    end
end
end
end
